package com.arcnor.timextractor;

import javax.imageio.ImageIO;
import java.awt.image.*;
import java.io.*;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.*;

public class Main {
	public static DataInput littleEndian(final DataInput decorated) {
		class LittleInput implements DataInput {
			private byte readBuffer[] = new byte[8];
			private ByteBuffer buffer = ByteBuffer.allocate(8);

			@Override
			public void readFully(byte[] b) throws IOException {
				decorated.readFully(b);
			}

			@Override
			public void readFully(byte[] b, int off, int len) throws IOException {
				decorated.readFully(b, off, len);
			}

			@Override
			public int skipBytes(int n) throws IOException {
				return decorated.skipBytes(n);
			}

			@Override
			public boolean readBoolean() throws IOException {
				return decorated.readBoolean();
			}

			@Override
			public byte readByte() throws IOException {
				return decorated.readByte();
			}

			@Override
			public int readUnsignedByte() throws IOException {
				return decorated.readUnsignedByte();
			}

			@Override
			public short readShort() throws IOException {
				int ch1 = decorated.readByte();
				int ch2 = decorated.readByte();
				return (short)((ch2 << 8) + ch1);
			}

			@Override
			public int readUnsignedShort() throws IOException {
				int ch1 = decorated.readUnsignedByte();
				int ch2 = decorated.readUnsignedByte();
				return (ch2 << 8) + ch1;
			}

			@Override
			public char readChar() throws IOException {
				return decorated.readChar();
			}

			public int readInt() throws IOException {
				buffer.clear();
				buffer.order(ByteOrder.BIG_ENDIAN)
						.putInt(decorated.readInt())
						.flip();
				return buffer.order(ByteOrder.LITTLE_ENDIAN)
						.getInt();
			}

			@Override
			public long readLong() throws IOException {
				readFully(readBuffer, 0, 8);
				return (((long)readBuffer[7] << 56) +
						((long)(readBuffer[6] & 255) << 48) +
						((long)(readBuffer[5] & 255) << 40) +
						((long)(readBuffer[4] & 255) << 32) +
						((long)(readBuffer[3] & 255) << 24) +
						((readBuffer[2] & 255) << 16) +
						((readBuffer[1] & 255) <<  8) +
						 (readBuffer[0] & 255));
			}

			@Override
			public float readFloat() throws IOException {
				return decorated.readFloat();
			}

			@Override
			public double readDouble() throws IOException {
				return decorated.readDouble();
			}

			@Override
			public String readLine() throws IOException {
				return decorated.readLine();
			}

			@Override
			public String readUTF() throws IOException {
				return decorated.readUTF();
			}

		}

		return new LittleInput();
	}

	private static class FileEntry {
		public int unk1, unk2, offset;
	}

	private static class FileEntries {
		public String fileName;
		public List<FileEntry> entries;
	}

	private static class ImageInfo {
		public int width, height;
	}

	public static void main(String[] args) throws IOException {
		if (args.length < 1) {
			System.err.println(String.format("Usage: timextractor <RESOURCE.MAP>"));
			return;
		}

		File f = new File(args[0]);
		if (!f.exists()) {
			System.err.println(String.format("File '%1$s' doesn't exist", args[0]));
			return;
		}
		String mainPath = f.getParent();

		DataInputStream stream = new DataInputStream(new BufferedInputStream(new FileInputStream(f)));
		DataInput dis = littleEndian(stream);
		int version = dis.readUnsignedShort();
		int unknown1 = dis.readUnsignedShort();
		int nFiles = dis.readUnsignedShort();
		System.out.println(String.format("Version: %1$d", version));
		System.out.println(String.format("Unknown1: %1$d", unknown1));
		System.out.println(String.format("Number of files: %1$d", nFiles));

		Map<String, byte[]> files = new HashMap<String, byte[]>();
		Map<String, IndexColorModel> pals = new HashMap<String, IndexColorModel>();
		for (int j = 0; j < nFiles; j++) {
			FileEntries file = readFile(dis);
			File mainFile = new File(mainPath, file.fileName);
			File dir = new File(mainFile.getAbsolutePath() + ".data");
			// FIXME: Remove existing directory first
			dir.mkdir();

			long mainFileSize = mainFile.length();
			DataInputStream fStream = new DataInputStream(new BufferedInputStream(new FileInputStream(mainFile)));
			DataInput fDis = littleEndian(fStream);
			int size = file.entries.size();
			for (int k = 0; k < size; k++) {
				FileEntry entry = file.entries.get(k);
				FileEntry nextEntry = k < (size - 1) ? file.entries.get(k + 1) : null;

				int offset = entry.offset;
				String name = readFileName(fDis);
				int fSize = nextEntry != null ? nextEntry.offset - offset : (int) (mainFileSize - offset);

				int expectedSize = fSize - 0x0D - 0x04;
				int readSize = fDis.readInt();
				if (readSize != expectedSize) {
					System.err.println(String.format("Size: Read %1$d, expected %2$d", readSize, expectedSize));
				}
				byte[] buffer = new byte[readSize];
				fDis.readFully(buffer);

				File tempF = new File(dir, name);
				tempF.createNewFile();
				FileOutputStream fos = new FileOutputStream(tempF);
				fos.write(buffer);
				fos.close();

				files.put(name, buffer);

				Mime mime = readMime(buffer);
				if (mime == Mime.PALETTE) {
					pals.put(name, createPalette(buffer));
				}
			}
			fStream.close();
		}
		stream.close();

		// Now, parse files
		File dir = new File(mainPath, "RESOURCES");
		// FIXME: Remove existing directory first
		dir.mkdir();
		for (Map.Entry<String, byte[]> entry : files.entrySet()) {
			String name = entry.getKey();
			byte[] buffer = entry.getValue();
			Mime mime = readMime(buffer);

			System.out.println(String.format("Name: %1$s\tMIME: %2$s", name, mime));
			switch (mime) {
				case BITMAP:
					parseBitmap(dir, name, buffer, pals);
			}
		}
	}

	private static void parseBitmap(File dir, String name, byte[] buffer, Map<String, IndexColorModel> pals) throws IOException {
		DataInput dis = littleEndian(new DataInputStream(new ByteArrayInputStream(buffer)));

		if (!isSection(dis, "BMP:")) return;
		int sectSize = dis.readUnsignedShort();
		int unk = dis.readUnsignedShort();

		if (!isSection(dis, "INF:")) return;
		sectSize = dis.readUnsignedShort();
		unk = dis.readUnsignedShort();
		int nInfo = dis.readUnsignedShort();
		System.out.println(String.format("\t%1$d images", nInfo));
		List<ImageInfo> imageInfos = new ArrayList<ImageInfo>();
		for (int j = 0; j < nInfo; j++) {
			ImageInfo imageInfo = new ImageInfo();
			imageInfo.width = dis.readUnsignedShort();

			imageInfos.add(imageInfo);
		}
		for (ImageInfo info : imageInfos) {
			info.height = dis.readUnsignedShort();
		}

		String dataSection = getSection(dis);
		sectSize = dis.readUnsignedShort();
		unk = dis.readUnsignedShort();
		IndexColorModel pal = pals.get("TIM.PAL");
		if (dataSection.equals("SCN:")) {
			System.out.println("\tScene?");
			int j = 0;
			for (ImageInfo info : imageInfos) {
				WritableRaster raster = decodeSCN(dis, info);
				if (raster == null) {
					break;
				}

				BufferedImage img = new BufferedImage(pal, raster, false, null);
				ImageIO.write(img, "PNG", new File(dir, name + "-" + j + ".png"));

				j++;
			}
		} else if (dataSection.equals("BIN:")) {
			System.out.println("\tBinary?");
		} else {
			System.out.println("\tUnknown section");
		}
	}

	private static WritableRaster decodeSCN(DataInput dis, ImageInfo imageInfo) throws IOException {
		WritableRaster raster = Raster.createInterleavedRaster(DataBuffer.TYPE_BYTE, imageInfo.width, imageInfo.height, 1, null);
		int palOffset = dis.readUnsignedByte();
		int x = 0, y = 0;
		int pixel;

		int lastGoBack = -1;

		try {
			while (true) {
				int cmdRaw = dis.readUnsignedByte();
				int cmd = cmdRaw >> 6;
				int cmdVal = cmdRaw & 0x3F;
				if (lastGoBack >= 0) {
					if (lastGoBack == 0 && cmdRaw == 0x40) {
						break;
					} else if (cmd == 0) {
						lastGoBack |= cmdVal << 6;
					}
					y++;
					x -= lastGoBack;
					lastGoBack = -1;
					if (cmd == 0) {
						continue;
					}
				}
				switch (cmd) {
					case 0:	// 0b00XXXXXX: Go back X pixels on the next line
						lastGoBack = cmdVal;
						break;
					case 1:	// 0b01XXXXXX: Skip the next X pixels
						x += cmdVal;
						break;
					case 2:	// 0b10XXXXXX: Draw X pixels using the next byte as color index
						pixel = dis.readUnsignedByte() + palOffset;
						for (int j = 0; j < cmdVal; j++) {
							raster.setSample(x++, y, 0, pixel);
						}
						break;
					case 3:	// 0b11XXXXXX: Draw the following X nibbles
						for (int j = 0; j < cmdVal; j += 2) {
							int b = dis.readUnsignedByte();
							int n1 = ((b & 0xF0) >> 4) + palOffset;
							int n2 = (b & 0x0F) + palOffset;
							raster.setSample(x++, y, 0, n1);
							if (j + 1 < cmdVal) {
								raster.setSample(x++, y, 0, n2);
							}
						}
						break;
				}
			}
		}
		catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
		return raster;
	}

	private static IndexColorModel createPalette(byte[] buffer) throws IOException {
		DataInput dis = littleEndian(new DataInputStream(new ByteArrayInputStream(buffer)));

		if (!isSection(dis, "PAL:")) return null;
		int sectSize = dis.readUnsignedShort();
		int unk = dis.readUnsignedShort();
		if (!isSection(dis, "VGA:")) return null;
		sectSize = dis.readUnsignedShort();
		unk = dis.readUnsignedShort();
		int nColors = (int) (sectSize / 3f);
		int[] pal = new int[nColors];
		for (int j = 0; j < nColors; j++) {
			int r = dis.readUnsignedByte();
			int g = dis.readUnsignedByte();
			int b = dis.readUnsignedByte();
			// Each component is (example with the maximum value):
			// 0x3F -> `(component << 2) + 3` The 3 comes from (component | 0x30) >> 4
			int color = 0xFF000000 |
					(r << (2 + 16)) + ((r & 0x30) >> 4 << 16) |
					(g << (2 + 8))  + ((g & 0x30) >> 4 << 8) |
					(b << 2)        + ((b & 0x30) >> 4);
			pal[j] = color;
		}

		return new IndexColorModel(8, pal.length, pal, 0, true, 0, DataBuffer.TYPE_BYTE);
	}

	private static boolean isSection(DataInput dis, String sectionName) throws IOException {
		byte[] section = new byte[4];
		dis.readFully(section);
		return Arrays.equals(section, sectionName.getBytes());
	}

	private static String getSection(DataInput dis) throws IOException {
		byte[] section = new byte[4];
		dis.readFully(section);
		return new String(section);
	}

	private static Mime readMime(byte[] buffer) {
		switch(buffer[0]) {
			case (byte)-0x13:
				if (buffer[1] == -0x54) {
					return Mime.LEVEL;
				}
				break;
			case 'B':
				if (buffer[1] == 'M' && buffer[2] == 'P')
					return Mime.BITMAP;
				break;
			case 'F':
				if (buffer[1] == 'N' && buffer[2] == 'T')
					return Mime.FONT;
				break;
			case 'O':
				if (buffer[1] == 'V' && buffer[2] == 'L')
					return Mime.OVL;
				break;
			case 'P':
				if (buffer[1] == 'A' && buffer[2] == 'L')
					return Mime.PALETTE;
				break;
			case 'S':
				switch (buffer[1]) {
					case 'C':
						if (buffer[2] == 'R')
							return Mime.SCREEN;
						break;
					case 'N':
						if (buffer[2] == 'D')
							return Mime.SOUND;
						break;
					case 'S':
						if (buffer[2] == 'M')
							return Mime.SSM;
						break;
				}
				break;
		}
		return Mime.UNKNOWN;
	}

	private static void readDataFile(DataInput fDis, int offset, int size) throws IOException {
		String fileName = readFileName(fDis);
		System.out.println(fileName);

		byte[] buffer = new byte[size];
		fDis.readFully(buffer, offset, size);
	}

	private static FileEntries readFile(DataInput dis) throws IOException {
		FileEntries result = new FileEntries();
		result.fileName = readFileName(dis);
		System.out.println(String.format("File: %1$s", result.fileName));

		int nFiles = dis.readUnsignedShort();

		result.entries = new ArrayList<FileEntry>(nFiles + 1);
		for (int j = 0; j < nFiles; j++) {
			FileEntry entry = new FileEntry();
			entry.unk1 = dis.readUnsignedShort();
			entry.unk2 = dis.readUnsignedShort();
			entry.offset = dis.readInt();
			result.entries.add(entry);
		}

		return result;
	}

	private static String readFileName(DataInput dis) throws IOException {
		byte[] fileName = new byte[0x0D];
		dis.readFully(fileName);
		int firstNull = 0;
		while (firstNull < fileName.length && fileName[firstNull] != 0) {
			firstNull++;
		}
		return (fileName[firstNull] == 0) ? new String(fileName, 0, firstNull) : null;
	}

	private static enum Mime {
		PALETTE, BITMAP, SCREEN, SOUND, FONT, LEVEL, SSM, OVL, UNKNOWN
	}
}
